# Docker

This project is a hotel room booking tool which allows you to search, reserve and cancel rooms.
## Build

execute the following command

```bash
git clone https://gitlab.com/maliossimo1/BDD_NOSQL.git
cd BDD_NOSQL
docker compose up
```
and **go to** http://localhost:5000.



## Project structure

This project uses  : 
* **Python3** container for the Flask app.
* **mongodb** container to store user iformation 
* **Postgresql** container to backup the data from the app

 the data of this app is persistant







