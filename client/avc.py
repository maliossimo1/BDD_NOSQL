from flask import Flask, session, redirect, url_for, request,render_template
import psycopg2
import requests
import time 
from pymongo import MongoClient

user = MongoClient(host='mongodb',port=27017,username='test',password='test')
mongodb=user.suggestion


app = Flask(__name__)

mydb = psycopg2.connect(
  host="db",
  user="postgres",
  password="postgres",
  database="postgres"
)
def username(name,password):
    addbd = { 
            "username":name, 
            "password":password,
            }
    if mongodb.table.find(addbd).count()>=1:
        test = mongodb.table.find(addbd)
        for x in test:
            return x["id"]
    else:
        return -1

@app.errorhandler(404) 
def invalid_route(e): 
    return redirect(url_for("login",url="index"))

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        if username(request.form["username"],request.form['password']) == -1:
            return redirect(url_for('login',url = "index"))
        session['username'] = request.form['username']
        session['password'] = request.form['password']
        return redirect(url_for(request.args.get('url')))
    return '''
        <form method="post">
            <p>username  <input type=text name=username>
            <p>password  <input type=password name=password>
            <p><input type=submit value=Login>
        </form>
    '''

@app.route('/singin', methods=['GET', 'POST'])
def singin():
    if request.method == 'POST':
        username_page= request.form["username"]
        if request.form["password"] != request.form["cpassword"]:
            return redirect(url_for('singin'))
        if username(username_page, request.form["password"]) != -1:
            return "l'utilisateur est deja existant"
        else:
            find_id = mongodb.table.count() + 1
            adddb = {
                "username": request.form["username"],
                "password" : request.form["password"],
                "id" : find_id
            }
            mongodb.table.insert_one(adddb)
            return redirect(url_for('login', url = "index"))

    return '''
        <form method="post">
            <p>username  <input type=text name=username>
            <p>date de naissance  <input type=date name=Ndate>
            <p>password  <input type=password name=password>
            <p>confirm password  <input type=password name=cpassword>
            <p><input type=submit value=confirm>
        </form>
    '''

@app.route('/filter',methods=['GET', 'POST'])
def index():
    
    if 'username' in session and 'password' in session:
        if request.method=="GET":
            return '''
        <form method="post">
            <p> Date de début
            <p><input type="date" id="start" name="start" required>
            <p> Nombre de nuit
            <p><input type=number name=nbrofnight required >
            <p> Nombre de chambres 
            <p><input type=number name=nbrroom required >
            <p><input type=submit value=Login>
        </form>
    '''
        url=f'http://tomcat:8080/Hotel/services/A/information?rentaldate={request.form["start"]}&numberofnights={request.form["nbrofnight"]}&numberofrooms={request.form["nbrroom"]}&username={session["username"]}&password={session["password"]}'
        myget=requests.get(url)
        
        start = '<ns:return>'
        end = '</ns:return>'
        s = myget.text
        if "FOUND" in s.upper():
            return s
        
        c=(s.split(start))[1].split(end)[0]
        html=""
        try:
            nom=c.split("Name:")
            for i in nom :
                if i!="":
                    html+=f'<p>{i}'
                    
            return html
        except:
            return myget.text
        
    return redirect(url_for("login",url="index"))


@app.route('/reserv',methods=['GET', 'POST'])
def reserv():
    
    if 'username' in session and 'password' in session:
        if request.method=="GET":
            return '''
        <form method="post">
            <p> Date de début
            <p><input type="date" id="start" name="start" required>
            <p> Nombre de nuit
            <p><input type=number name=nbrofnight required >
            <p> Nombre de chambres 
            <p><input type=number name=nbrroom required >
            <p> Hotelid 
            <p><input type=number name=idhotel required >
            <p><input type=submit value=Login>
        </form>
    '''
        url=f'http://tomcat:8080/Hotel/services/A/reservation?rentaldate={request.form["start"]}&numberofnights={request.form["nbrofnight"]}&numberofrooms={request.form["nbrroom"]}&hotelid={request.form["idhotel"]}&username={session["username"]}&password={session["password"]}'
        myget=requests.get(url)
        mydb.commit()
        if "True" in myget.text:
            return "Reservation successful"
        return  f'Reservation error or the hotel {request.form["idhotel"]} is not available'
        
    return redirect(url_for("login",url="reserv"))

@app.route('/cancel',methods=['GET', 'POST'])
def cancel():
    
    if 'username' in session and 'password' in session:
        if request.method=="GET":
            mycursor = mydb.cursor()
            client_id=username(session["username"],session["password"])
            mycursor.execute('SELECT * FROM reservation where client_id=%s',(int(client_id),))
            myresult = mycursor.fetchall()
            s=[]
            for x in myresult:
                print(x)
                s.append(" ".join(str(i) for i in x))
            return render_template('cancelget.html',reserv=s)

        annulation=request.form.getlist('reservation')
        for i in annulation:
            id=i.split(" ")
            url=f'http://tomcat:8080/Hotel/services/A/cancel?id={id[0]}&username={session["username"]}&password={session["password"]}'
            myget=requests.get(url)
            
            if "False" in myget.text:
                return "False"
        mydb.commit()
        return "True"
        
    return redirect(url_for("login",url="cancel"))
@app.route('/logout')
def logout():
    # remove the username from the session if it's there
    session.pop('username', None)
    return redirect(url_for('login',url="index"))



# set the secret key.  keep this really secret:
app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'

if __name__ == '__main__':
    app.run(host="0.0.0.0",port=5000)