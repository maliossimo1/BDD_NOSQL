--
-- PostgreSQL database dump
--

-- Dumped from database version 13.1
-- Dumped by pg_dump version 13.1

-- Started on 2020-11-26 18:38:01

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;------------------------

CREATE TABLE public.client
(
    id serial NOT NULL,
    username text NOT NULL,
    passeword text NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE public.client
    OWNER to postgres;

INSERT INTO public.client(
	id, username, passeword)
	VALUES (1,'malek','12345');
 INSERT INTO public.client(
	id, username, passeword)
	VALUES (2,'omar','kotti');


CREATE TABLE public.hotel
(
    id serial NOT NULL,
    name text NOT NULL,
    adress text NOT NULL,
    stars integer NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE public.hotel
    OWNER to postgres;

INSERT INTO public.hotel(
	id, name, adress, stars)
	VALUES (1, 'Brigitte', 'chez ta mere', 5);

INSERT INTO public.hotel(
	id, name, adress, stars)
	VALUES (2, 'formula1', 'chez ton pere', 4);



CREATE TABLE public.chambre
(
    id serial NOT NULL,
    hotel_id integer NOT NULL,
    number_of_nights integer NOT NULL,
    departure_date date NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (hotel_id)
        REFERENCES public.hotel (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
);

ALTER TABLE public.chambre
    OWNER to postgres;

INSERT INTO public.chambre(
	id, hotel_id, number_of_nights, departure_date)
	VALUES (1, 1, 5, '2020-11-08');

INSERT INTO public.chambre(
	id, hotel_id, number_of_nights, departure_date)
	VALUES (2, 2, 6, '2020-11-08');


CREATE TABLE public.reservation
(
    id serial NOT NULL,
    hotel_id integer NOT NULL,
    chambre_id integer NOT NULL,
    departure_date date NOT NULL,
    arrival_date date NOT NULL,
    client_id integer,
    PRIMARY KEY (id),
    FOREIGN KEY (hotel_id)
        REFERENCES public.hotel (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    FOREIGN KEY (chambre_id)
        REFERENCES public.chambre (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
   
);

ALTER TABLE public.reservation
    OWNER to postgres;
